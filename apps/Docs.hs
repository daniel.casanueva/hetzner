
module Main (main) where

-- base
import Data.Foldable (forM_)
import Data.List (sortOn)
import Data.Maybe (isNothing)
-- hetzner
import Hetzner.Cloud qualified as Hetzner
-- bytestring
import qualified Data.ByteString.Lazy as ByteString
-- directory
import System.Directory (createDirectoryIfMissing)
-- blaze-html
import Text.Blaze.Html (toHtml, preEscapedToHtml, (!))
import Text.Blaze.Html.Renderer.Utf8 (renderHtml)
import Text.Blaze.Html5 qualified as H
import Text.Blaze.Html5.Attributes qualified as A
-- time
import Data.Time.Clock.POSIX (getPOSIXTime)

main :: IO ()
main = do
  mtoken <- Hetzner.getTokenFromEnv
  token <- maybe (fail "HETZNER_API_TOKEN is not set") pure mtoken
  now <- round <$> getPOSIXTime :: IO Int
  -- Server types
  let serverTypesFP = "public/server-types"
  createDirectoryIfMissing True serverTypesFP
  serverTypes0 <- Hetzner.streamToList $ Hetzner.streamPages $ Hetzner.getServerTypes token
  let maxPrice = maximum . (:) 0
               . fmap (Hetzner.grossPrice . Hetzner.monthlyPrice)
               . Hetzner.serverPricing
  let serverTypes = sortOn maxPrice $ filter (not . Hetzner.serverDeprecated) serverTypes0
  ByteString.writeFile (serverTypesFP ++ "/index.html") $ renderHtml $ H.docTypeHtml $ do
    H.head $ do
      H.title "Hetzner Server Type List"
      H.link ! A.rel "stylesheet" ! A.href "https://use.typekit.net/yxf7kgr.css"
      H.link ! A.rel "stylesheet" ! A.href "../common.css"
    H.body $ do
      H.h1 "Hetzner Server Type List"
      H.hr
      H.table $ do
        H.tr ! A.class_ "hrow" $ do
          H.th "Server Type ID"
          H.th "Name"
          H.th "Architecture"
          H.th "Cores"
          H.th "Memory"
          H.th "Disk"
          H.th "CPU Type"
          H.th "Max Monthly Price"
        forM_ serverTypes $ \serverType -> H.tr $ do
          H.td $ (\(Hetzner.ServerTypeID i) -> toHtml i) $ Hetzner.serverTypeID serverType
          H.td $ toHtml $ Hetzner.serverTypeName serverType
          H.td $ toHtml $ show $ Hetzner.serverArchitecture serverType
          H.td $ toHtml $ Hetzner.serverCores serverType
          H.td $ toHtml $ show (Hetzner.serverMemory serverType) ++ " GB"
          H.td $ toHtml $ show (Hetzner.serverDisk serverType) ++ " GB"
          H.td $ toHtml $ case Hetzner.serverCPUType serverType of
            Hetzner.SharedCPU -> "Shared" :: String
            Hetzner.DedicatedCPU -> "Dedicated"
          H.td $ toHtml $ show (maxPrice serverType) ++ " €"
    H.p $ H.em $ do
      "Updated "
      H.script $ preEscapedToHtml $
        "const date = new Date(" ++ show (now * 1000) ++ "); "
        ++ "document.write(date.toLocaleString());"
      "."
  -- Images
  let imagesFP = "public/images"
  createDirectoryIfMissing True imagesFP
  images0 <- Hetzner.streamToList $ Hetzner.streamPages $ Hetzner.getImages token
  let images = sortOn (show . Hetzner.imageName)
             $ filter (isNothing . Hetzner.imageDeprecated) images0
  print images
  ByteString.writeFile (imagesFP ++ "/index.html") $ renderHtml $ H.docTypeHtml $ do
    H.head $ do
      H.title "Hetzner Image List"
      H.link ! A.rel "stylesheet" ! A.href "https://use.typekit.net/yxf7kgr.css"
      H.link ! A.rel "stylesheet" ! A.href "../common.css"
    H.body $ do
      H.h1 "Hetzner Image List"
      H.p "These are the images available by default. Your project might have other images available in addition to these."
      H.hr
      H.table $ do
        H.tr ! A.class_ "hrow" $ do
          H.th "Image ID"
          H.th "Name"
          H.th "OS Flavor"
          H.th "Architecture"
          H.th "Size"
        forM_ images $ \image -> H.tr $ do
          H.td $ (\(Hetzner.ImageID i) -> toHtml i) $ Hetzner.imageID image
          H.td $ toHtml $ Hetzner.imageName image
          H.td $ toHtml $ show $ Hetzner.imageOSFlavor image
          H.td $ toHtml $ show $ Hetzner.imageArchitecture image
          H.td $ toHtml $ show (Hetzner.imageDiskSize image) ++ " GB"
    H.p $ H.em $ do
      "Updated "
      H.script $ preEscapedToHtml $
        "const date = new Date(" ++ show (now * 1000) ++ "); "
        ++ "document.write(date.toLocaleString());"
      "."
