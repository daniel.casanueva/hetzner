# Hetzner Haskell Library

## Description

Client interface for the [Hetzner Cloud API](https://docs.hetzner.cloud)
and the [Hetzner DNS API](https://dns.hetzner.com/api-docs),
written in the [Haskell](https://www.haskell.org) programming language.

## Completeness

This library doesn't cover (at least, not yet) the enterity of Hetzner's API.
However, if there's anything you would like to see added, feel free to
[open an issue](https://gitlab.com/daniel-casanueva/haskell/hetzner/-/issues/new).
If it's urgent, send me an email.
I mainly add features only when I myself need them, but I'd be happy to extend the
library upon request. Of course, if you can contribute with a merge request,
that'd be greatly appreciated. The only ask I have if you decide to contribute
is to follow more or less the naming and styling already used in the rest of
the library. Thanks!

## Documentation

Find documentation from the main (potentially unreleased) branch
[here](https://daniel-casanueva.gitlab.io/haskell/hetzner/doc).
